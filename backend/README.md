## Acerca de

## Sobre el proyecto

Este proyecto es el cliente api que se conecta al frontend registrado en el proyecto: [frontend]

## Encargados del Proyecto

### Desarrollo
- Ezio Olviera (eliuolivera97@gmail.com)

## Requisitos previos para iniciar la aplicación.

* Antes de comenzar, asegúrese de que su entorno de desarrollo incluya React.js y un administrador de paquetes npm.

* Python
    * Este proyecto requiere python  versión 3.10 o posterior.



* Ingrese a la carpeta raíz del proyecto e instale las dependencias con el siguiente comando.

```
python -m pip install --upgrade pip
```

* Para iniciar la aplicación necesitamos levantar un entorno virutal.

```
python -m pip install virtualenv
python -m virtualenv env

```

* Ingresamos a la siguiente ruta para activar el entorno virtual.

```
.\env\Scripts\activate

```

* De ser necesario algunos podemos ejecutar el siguiente comando en el CMD.

```
Get-ExecutionPolicy -List 
Set-ExecutionPolicy RemoteSigned -Scope CurrentUser 
```

* Necesitamos Instalar las siguientes dependencias.

```
pip install djangorestframework
pip install djangorestframework-jsonapi
pip install django-filter
python -m pip install requests
pip freeze
```

* Una vez terminado podemos levantar el entorno virtual.

```
python manage.py run server
```

* Se inicializara el proyecto con la siguiente ruta.

```
http://localhost:8000/

```

* Ya podemos usar nuestro proyecto.



