from django.urls import path
from . import views

urlpatterns = [
    
    path('getClientRut/<rut>/',views.getClientRut),
    path('putTransferClient/',views.putTransferClient),
    path('getIndicador/<valor>/',views.IndEconomic),

]
