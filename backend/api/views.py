from rest_framework.response import Response
from rest_framework.serializers import Serializer
from rest_framework.decorators import api_view
from rest_framework import status
from .models import Clients
from .models import Users
from .serializers import UsersSerializer
from .serializers import ClientsSerializer
from django.db import transaction,IntegrityError
from django.shortcuts import render
import requests
import json
import datetime

 
@api_view(['GET'])
def getClientRut(request,rut):
    try:
        client = Clients.objects.get(rut=rut)
        if client:
            serializer = ClientsSerializer(client, many= False)
            return Response(serializer.data)
        return Response("Ocurrio algo en el proceso")
    except Clients.DoesNotExist:
        return Response("El RUT ingresado no se encuentra registrado")

@api_view(['POST'])
def putTransferClient(request):
    try :
        data = request.data
        account_ori = Clients.objects.get(account_number=data['account_number'])
        account_dest = Clients.objects.get(account_number=data['account_dest'])
        #if account_ori == None:
            #raise Response("No existe la cuenta origen", status = status.HTTP_400_BAD_REQUEST) 
        account_balance = account_ori.balance
        if data['amount'] < account_balance:
            account_ori.balance = account_balance - data['amount'] 
        else:
            return Response("No cuenta con saldo suficiente" , status = status.HTTP_400_BAD_REQUEST)
        
        if account_dest == None:
            raise Response("No existe la cuenta destino", status = status.HTTP_400_BAD_REQUEST) 
        account_dest.balance = account_dest.balance + data['amount']
          
        serializer_origen = ClientsSerializer(instance=account_ori,data={'balance':account_ori.balance },partial= True)
        serializer_destino = ClientsSerializer(instance=account_dest,data={'balance':account_dest.balance },partial= True)

        if serializer_origen.is_valid() and serializer_destino.is_valid():
            serializer_origen.save()
            serializer_destino.save()
            return Response("Transferencia exitosa", status = status.HTTP_201_CREATED)
        else :
            return Response("Ocurrio un error al actualizar los datos")
    except IntegrityError:
        return Response("Error al transferir fondos",status = status.HTTP_400_BAD_REQUEST)
    except Clients.DoesNotExist:
        return Response("No existe la cuenta origen / destino",status = status.HTTP_400_BAD_REQUEST)
    
@api_view(['GET'])  
def IndEconomic(request,valor):
    try:
        valor = valor
        date = datetime.date.today()
        formatedDate = date.strftime("%d-%m-%Y")

        response = requests.get('https://mindicador.cl/api/'+ valor + '/' + formatedDate )
        data = json.loads(response.text.encode("utf-8"))
        pretty_json = json.dumps(data, indent=2)
        return Response(data)
    except:
        return Response("Ocurrio un error al capturar los datos",status = status.HTTP_400_BAD_REQUEST)