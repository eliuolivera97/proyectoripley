from django.db import models

class Users(models.Model):
    name = models.CharField(max_length=30)
    email = models.EmailField(max_length = 100)
    password = models.TextField()
    audit_user_create = models.IntegerField()
    audit_user_create_date = models.DateTimeField()
    audit_user_update = models.IntegerField(null=True,blank=True)
    audit_user_update_date =  models.DateTimeField(null=True,blank=True)
    
class Clients(models.Model):
    name = models.CharField(max_length=30)
    email = models.EmailField(max_length = 100)
    rut = models.TextField()
    balance = models.DecimalField(max_digits=10, decimal_places=2)
    account_number = models.IntegerField()
    audit_user_create = models.IntegerField()
    audit_user_update = models.IntegerField(null=True,blank=True)
    audit_user_create_date = models.DateTimeField()
    audit_user_update_date =  models.DateTimeField(null=True,blank=True)


