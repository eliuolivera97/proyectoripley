from rest_framework.serializers import ModelSerializer
from .models import Users
from .models import Clients


class UsersSerializer(ModelSerializer):
    class Meta:
        model = Users
        fields = '__all__'

class ClientsSerializer(ModelSerializer):
    class Meta:
        model = Clients
        fields = '__all__'
