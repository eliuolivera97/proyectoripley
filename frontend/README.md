## Acerca de

## Sobre el proyecto

Este proyecto es el cliente web que se conecta a las APIs registradas en el proyecto: [backend]

## Encargados del Proyecto

### Desarrollo
- Ezio Olviera (eliuolivera97@gmail.com)

## Requisitos previos para iniciar la aplicación.

* Antes de comenzar, asegúrese de que su entorno de desarrollo incluya React.js y un administrador de paquetes npm.

* React.js
    * Este proyecto requiere react.js versión 18.2.0 o posterior.

* NPM
    * Este proyecto requiere NPM versión 6.13.4 o posterior.
`

* Ingrese a la carpeta raíz del proyecto e instale las dependencias con el siguiente comando.

```
npm install
```

* Inicie la aplicación con el siguiente comando.

```
npm start
```

* Se abrirá automaticamente una pestaña en su navegador en la dirección.

```
http://localhost:3000/
```
