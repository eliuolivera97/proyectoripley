import React, {useState,useEffect} from "react";

// Components
import Header from './components/Header';
import ClientForm from "./components/ClientTransferForm";
import ClientConsultaSaldo from "./components/ClientConsultaSaldo";
import IndicadorEconomico from "./components/IndicadorEconomico";
import axios from "axios";
import Button from 'react-bootstrap/Button';


function App() {

  const [clients,setClient]= useState([]);
  const [show1, setShow1] = useState(false);
  const [show2, setShow2] = useState(false);
  const [show3, setShow3] = useState(false);

  return (
    <>
    <Header/>
    <div className="container p-4">
      <div className="container p-4">
        <Button variant='dark'type="button"
          onClick={() => {
            setShow1(!show1);
          }}
          
          >
          Realiza Transferencia  {show1 ? 'Ocultar' : ''}
        </Button>
        {show1 ? (
            <ClientForm client={clients} setClient={setClient} />
          ) : (  false   )}

      </div>
      <div className="container p-4">
        <Button variant='dark'type="button"
          onClick={() => {
            setShow2(!show2);
          }}
          
          >
          Consulta Saldo  {show2 ? 'Ocultar' : ''}

        </Button>
      {show2 ? (
          <ClientConsultaSaldo client={clients} setClient={setClient} />
        ) : (  false  )}
      
      </div>

      <div className="container p-4">
        <Button variant='dark'type="button"
          onClick={() => {
            setShow3(!show3);
          }}
          
          >
          Consulta Indicador  {show3 ? 'Ocultar' : ''}

        </Button>
      {show3 ? (
          <IndicadorEconomico client={clients} setClient={setClient} />
        ) : (  false  )}
      
      </div>  
       

      </div>
    </>
  );
}

export default App;
