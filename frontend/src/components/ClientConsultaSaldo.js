import React, {useState} from "react";
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import axios from 'axios';
import Swal from "sweetalert2";  

export default function ClientConsultaSaldo ({clients, setClients}){

    
    const [rutClient,setRutClient] = useState('');

    const handleChange = e =>{
        setRutClient(e.target.value);

    }
    const handleSubmit = e => {

        e.preventDefault();
        if (!rutClient){
            return Swal.fire({  
                title: 'Error',  
                type: 'warning',  
                text: 'Ingrese Los campos requeridos',  
            });  
        }
        axios.get('/getClientRut/' + rutClient + '/', {
            rutClient:parseInt(rutClient),

        }).then((response) => {
            console.log(response.data)
            var texto = '';
            if (response.data.rut){
                texto =  'Rut: ' + response.data.rut + ' - Cliente ' + response.data.name + ' - Saldo S/. ' + response.data.balance;
            }else{
                texto = response.data;
            }
            return Swal.fire({  
                title: 'Success',  
                type: 'success',  
                text: texto ,  
            });  

        }).catch((error) =>{
           console.log(error)
            return Swal.fire({  
                title: 'Error',  
                type: 'warning',  
                text: error.response,  
            });  
        })
    }
    return (
        <Form onSubmit={handleSubmit}>
            <div className="container p-2">
                <InputGroup className='mb-3'>
                    <FormControl
                    onChange={handleChange}
                    value={rutClient}
                    type='int'
                    placeholder="Rut Cliente"
                    />
                </InputGroup>
                <Button variant='dark' type='submit'>
                    Consultar Saldo
                </Button>
            </div>
            
        </Form>
    )
}