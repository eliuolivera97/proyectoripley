import React, {useState} from "react";
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import axios from 'axios';
import Swal from "sweetalert2";  

export default function IndicadorEconomico ({clients, setClients}){

    
    const [indicador,setIndicador] = useState('');

    const handleChange = e =>{
        setIndicador(e.target.value);

    }
    const handleSubmit = e => {

        e.preventDefault();
        if (!indicador){
            return Swal.fire({  
                title: 'Error',  
                type: 'warning',  
                text: 'Ingrese Los campos requeridos',  
            });  
        }
        axios.get('/getIndicador/' + indicador + '/', {
            indicador:indicador,

        }).then((response) => {
            console.log(response.data)
            var texto = '';
            if (response.data.codigo){
                texto =  'Codigo: ' + response.data.codigo + ' - Nombre ' + response.data.nombre + ' - Unidad Medida ' + response.data.unidad_medida
                 + ' - Valor ' + response.data.serie[0].valor;
            }else{
                texto = response.data.message;
            }
            return Swal.fire({  
                title: response.data.codigo ? ('Success') : ( 'Warning'),  
                type: response.data.codigo ? ('Success') : ( 'Warning'),  
                text: texto ,  
            });  

        }).catch((error) =>{
           console.log(error)
            return Swal.fire({  
                title: 'Error',  
                type: 'warning',  
                text: error.data.message,  
            });  
        })
    }
    return (
        <Form onSubmit={handleSubmit}>
            <div className="container p-2">
                <InputGroup className='mb-3'>
                    <FormControl
                    onChange={handleChange}
                    value={indicador}
                    type='string'
                    placeholder="Indicadores ejm  uf,dolar,utm"
                    />
                </InputGroup>
                <Button variant='dark' type='submit'>
                    Consultar Indicador
                </Button>
            </div>
            
        </Form>
    )
}