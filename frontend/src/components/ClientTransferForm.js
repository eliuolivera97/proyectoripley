import React, {useState} from "react";
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import axios from 'axios';
import Swal from "sweetalert2";  

export default function ClientTransferForm ({clients, setClients}){

    
    const [account_number,setAccount_number] = useState('');
    const [account_dest,setAccount_dest] = useState('');
    const [amount,setAmount] = useState('');

    const handleChangeOrig = e =>{
        setAccount_number(e.target.value);

    }
    const handleChangeDest = e =>{
        setAccount_dest(e.target.value);

    }
    const handleChangeAmount= e =>{
        setAmount(e.target.value);

    }
    const handleSubmit = e => {

        e.preventDefault();
        if (!account_number || !account_dest || !amount){
            return Swal.fire({  
                title: 'Error',  
                type: 'warning',  
                text: 'Ingrese Los campos requeridos',  
            });  
        }
        axios.post('/putTransferClient/', {
            account_number:parseInt(account_number),
            account_dest:parseInt(account_dest),
            amount:parseInt(amount),

        }).then((response) => {
            console.log(response.data)
            setAccount_number('');
            setAccount_dest('');
            setAmount('');
            return Swal.fire({  
                title: 'Success',  
                type: 'success',  
                text: response.data,  
            });  

        }).catch((error) =>{
           
            return Swal.fire({  
                title: 'Error',  
                type: 'warning',  
                text: error.response.data,  
            });  
        })
    }


    return (
        <Form onSubmit={handleSubmit}>
            <div className="container p-2">
            <InputGroup className='mb-3'>
                <FormControl
                onChange={handleChangeOrig}
                value={account_number}
                type='int'
                placeholder="Cuenta Origen"
                />
            </InputGroup>
            <InputGroup className='mb-3'>
                <FormControl
                onChange={handleChangeDest}
                value={account_dest}
                type='int'
                placeholder="Cuenta Destino"
                />
            </InputGroup>
            <InputGroup className='mb-3'>
                <FormControl
                onChange={handleChangeAmount}
                value={amount}
                type='int'
                placeholder="Monto a transferir"
                />
            </InputGroup>
            <Button variant='dark' type='submit'>
                Enviar Transferencia
            </Button>
            </div>
            
        </Form>
    )
}